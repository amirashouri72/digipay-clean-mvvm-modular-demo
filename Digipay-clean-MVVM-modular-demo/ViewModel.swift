//
//  ViewModel.swift
//  digipay-ios-uikit-test
//
//  Created by AmirReza Ashouri on 8/15/21.
//

import Foundation

protocol ViewModelProtocol {
    var pinProviderBindable: Bindable<Bool> { get }

    var pinQueue: PinQueueProviding { get }
    
    func luncPinView()
    
    func pinProviderFetchedPinSuccessfuly()
    
    func pinProviderFailedToFetchedPin()
}

extension ViewModelProtocol {
    
    func luncPinView() {
        print("pin view lunched".capitalized)
        pinProviderBindable.value = true
    }
    
    func pinProviderFetchedPinSuccessfuly() {
        print("pin got fetched successfuly")
        pinQueue.execute()
    }
    
    func pinProviderFailedToFetchedPin() {
        luncPinView()
    }
}

public class ViewModel: ViewModelProtocol {
    
    var pinQueue: PinQueueProviding
    let pinProviderBindable = Bindable<Bool>()
    
    private var shouldPass = false
    private let domain = Domain()

    init(queue: PinQueueProviding) {
        pinQueue = queue
    }
    
    func doSomeUseCase(_ completion: @escaping (Result<Bool, PinError>) -> Void) {
        domain.veryComplexUseCase(shouldPass) { [weak self] res in
            guard let self = self else { return }
            switch res {
            case .success(_):
                print("domain complex function done")
            case .failure(let err):
                print("domain complex function failed because ", err)
                self.shouldPass = true
                self.pinQueue.add({self.doSomeUseCase(completion)})
                self.luncPinView()
            }
        }
    }
}

class Domain {
    func veryComplexUseCase(_ shouldPass: Bool, completion: @escaping (Result<Bool, PinError>) -> Void) {
        if shouldPass {
            completion(.success(true))
            print("passed use case")
        }else {
            print("failed use case")
            completion(.failure(.unAuhtorized))
        }
    }
}
