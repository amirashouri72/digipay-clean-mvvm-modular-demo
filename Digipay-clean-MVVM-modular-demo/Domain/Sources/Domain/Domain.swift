public class C2CUseCase {
    let name: String

    public init(name: String) {
        self.name = name
    }
    
    public func getMyName() -> String {
        return name
    }
}
