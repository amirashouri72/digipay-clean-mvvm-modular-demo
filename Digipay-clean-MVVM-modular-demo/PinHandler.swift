//
//  PinHandler.swift
//  digipay-ios-uikit-test
//
//  Created by AmirReza Ashouri on 8/15/21.
//

import UIKit

enum PinError: Error {
    case network
    case unAuhtorized
}

protocol PinProviding {
    func getPin() -> String
    func lunchPinController(in viewController: UIViewController?, completion: @escaping (Result<Bool, PinError>) -> Void)
}

class PinProvider: PinProviding {
    func getPin() -> String {
        return "token"
    }
    
    func lunchPinController(in viewController: UIViewController?, completion: @escaping (Result<Bool, PinError>) -> Void) {
        completion(.success(true))
    }
}

struct Job<T>: Equatable {
    static func == (lhs: Job, rhs: Job) -> Bool {
        lhs.id == rhs.id
    }
    
    let id = UUID().uuidString
    var callBack: ((T?)->())
}
