//
//  PinQueue.swift
//  digipay-ios-uikit-test
//
//  Created by AmirReza Ashouri on 8/15/21.
//

import Foundation


protocol PinQueueProviding {
    typealias Callback = () -> Void

    var tasks: [Callback] { get set }

    func add(_ task: @escaping Callback)

    func remove(_ task:  Callback)

    func execute()
}

class PinQueueProvider: PinQueueProviding {
    
    var tasks = [Callback]()

    func execute() {
        tasks.forEach({$0()})
    }

    func add(_ task: @escaping () -> Void) {
        tasks.append(task)
    }

    func remove(_ task: Callback) {
        tasks = tasks.filter({$0() == task()})
    }
}
