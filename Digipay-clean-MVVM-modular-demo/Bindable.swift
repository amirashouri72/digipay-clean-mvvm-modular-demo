//
//  Bindable.swift
//  Digipay-clean-MVVM-modular-demo
//
//  Created by AmirReza Ashouri on 8/23/21.
//

import Foundation

public class Bindable<T> {
    
    public var value: T? {
        didSet {
            observer?(value)
        }
    }
    
    public init() {}
    
    public var observer: ((T?)->())?
    
    public func bind(observer: @escaping (T?) ->()) {
        self.observer = observer
    }
}
