//
//  ViewController.swift
//  digipay-ios-uikit-test
//
//  Created by AmirReza Ashouri on 8/14/21.
//

import UIKit
import Domain
import Repository

class ViewController: UIViewController {
    
    let viewModel: ViewModel
    let pinProvider: PinProviding
    
    let someJobButton: UIButton = {
        let bt = UIButton()
        bt.setTitle("do the job", for: .normal)
        bt.addTarget(self, action: #selector(someJobButtonHandler), for: .touchUpInside)
        return bt
    }()
    
    init(_ vm: ViewModel, pp: PinProviding) {
        viewModel = vm
        pinProvider = pp
        super.init(nibName: nil, bundle: nil)
        observers()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        view.addSubview(someJobButton)
        
        let c2cUseCase = C2CUseCase(name: "c2c use case")
        print(c2cUseCase.getMyName())
        
        let c2cRepository = C2CRepository(name: "c2c repo")
        print(c2cRepository.getMyName())
    }
    
    @objc private func someJobButtonHandler() {
        viewModel.doSomeUseCase { res in
            
        }
    }
    
    func observers() {
        viewModel.pinProviderBindable.bind { [weak self, pinProvider, viewModel] _ in
            pinProvider.lunchPinController(in: self) { res in
                viewModel.pinProviderFetchedPinSuccessfuly()
            }
        }
    }
}
