public class C2CRepository {
    let name: String

    public init(name: String) {
        self.name = name
    }
    
    public func getMyName() -> String {
        return name
    }
}
